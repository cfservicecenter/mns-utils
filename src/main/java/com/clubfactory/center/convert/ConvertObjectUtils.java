package com.clubfactory.center.convert;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.clubfactory.center.common.util.BaseUtil;
import com.clubfactory.center.common.util.JSONUtil;
import com.clubfactory.center.dto.kafka.product.FbNationsInfoOfProductSyncDTO;
import com.clubfactory.center.dto.kafka.product.ProductImagesRequestDTO;
import com.clubfactory.center.dto.kafka.product.ProductTemplateRequestDTO;
import com.clubfactory.center.dto.kafka.product.SkuAttrSkuAttrValueDTO;
import com.clubfactory.center.enums.ProductActiveEnum;
import com.clubfactory.center.enums.ProductElectricEnum;
import com.clubfactory.product.center.client.dto.product.CategoryDTO;
import com.clubfactory.product.center.client.dto.product.ProductDTO;
import com.clubfactory.product.center.client.dto.product.SkuAttributeDTO;
import com.clubfactory.product.center.client.dto.product.SkuDTO;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @ClassName:ConvertObjectNew
 * @Description: Spring boot 规约大于配置
 * @Author kof.wang
 * @Date : 2019-07-12 19:06
 * @Version 1.0.0
 **/
@Slf4j
public class ConvertObjectUtils {

    private static final String s3Url = "https://s3.amazonaws.com/fromfactory.club.image";


    /**
     * @param productDTO
     * @param skuDTOS
     * @param multiLevelCategory
     * @return
     */
    // DTO to kafka message
    // 初始化失败：返回null
    // 非必要校验：返回dto，但是打印error日志 ：
    public static ProductTemplateRequestDTO convertKafkaDTO(
            ProductDTO productDTO,   /* 商品基本信息 */
            List<SkuDTO> skuDTOS,    /* 商品sku信息*/
            List<CategoryDTO> multiLevelCategory,      /* 商品类目信息 */
            Map<String, Double> exchangeRates) {

        if (BaseUtil.isEmpty(productDTO.getProductId()) || BaseUtil.isEmpty(productDTO.getProductNo())
                || BaseUtil.isEmpty(productDTO.getCategoryId()) || BaseUtil.isEmpty(multiLevelCategory)) {

            log.error("convertKafkaDTO error  : productId is [{}] ,productNo is  [{}] ,leafCategoryId is [{}]， multiLevelCategory is [{}]"
                    , productDTO.getProductId()
                    , productDTO.getProductNo(), productDTO.getCategoryId(), JSONObject.toJSONString(multiLevelCategory));
            return null;
        }

        ProductTemplateRequestDTO kafkaDTO = new ProductTemplateRequestDTO();
        kafkaDTO.setId(productDTO.getProductId());   // 商品spu Id
        kafkaDTO.setProduct_no(productDTO.getProductNo());   // 商品货号
        // 商品对应的叶子类目的id集合
        spuCategoryBulider(kafkaDTO, productDTO);
        // 商品对应的多级类目的id集合
        spuAllThirdCategoryBulider(kafkaDTO, multiLevelCategory);
        // 商品上下架状态
        spuActiveBuilder(kafkaDTO, productDTO);

        kafkaDTO.setName(productDTO.getName()); // 商品标题
        kafkaDTO.setImage_url(productDTO.getImageUrl()); //商品主图
        kafkaDTO.setOrders(productDTO.getOrders()); // 销量
        // spu兜底价格；取商品的印度价格中sku维度的价格最大值，数据来源价格计算
        spuList_priceBuilder(kafkaDTO, productDTO);
        // todo 删掉注释代码 多国兜底价格 和 多国划线价价格：
        // spuNew_list_priceAndMarking_priceBuilder(kafkaDTO, productDTO, skuDTOS, exchangeRates);
        // 商品图片
        spuProduct_imagesBuilder(kafkaDTO, productDTO);
        // 商品sku信息
        skuBuilder(kafkaDTO, productDTO, skuDTOS, exchangeRates);
        // companyId
        spuFactory_idBuilder(kafkaDTO, productDTO);

        // 是否带电
        spuElectricBuilder(kafkaDTO, productDTO);
        kafkaDTO.setB_average_rating(productDTO.getBAverageRating());
        kafkaDTO.setB_specifics(productDTO.getBSpecifics()); // 商品详细信息
        kafkaDTO.setOverview(productDTO.getOverview()); //
        kafkaDTO.setC_platform_price(productDTO.getCPlatformPrice()); // 划线价格，多个国家价格
        kafkaDTO.setC_platform_name(productDTO.getCPlatformName()); // C端平台名称
        kafkaDTO.setC_details(productDTO.getCDetails()); // c端详情
        kafkaDTO.setBenchmark_price(productDTO.getBenchmarkPrice());  // 标签价格
        kafkaDTO.setDaily_testing_flag(productDTO.getDailyTestingFlag()); // 0,默认值，1，测试中，2，满足测试标准
        kafkaDTO.setSearch_keywords(productDTO.getSearchKeywords()); // 商品搜索关键字
        kafkaDTO.setCreated_time(productDTO.getCreateDate().getTime()); //商品创建时间（第一次上架时间）
        kafkaDTO.setUpdated_time(productDTO.getWriteDate().getTime()); //商品更新时间
        kafkaDTO.setIllegal_tags(productDTO.getIllegalTags()); //商品标签集合
        kafkaDTO.setBiz_model(productDTO.getBizModel()); // 支撑模式 直发 自营
        kafkaDTO.setSource_id(productDTO.getSourceId()); //  商品来源??
        kafkaDTO.setSeller_id(productDTO.getSellerId()); //seller 卖家的id
        kafkaDTO.setSeller_name(productDTO.getSellerName()); //seller卖家的名称
        kafkaDTO.setSeller_country(productDTO.getSellerCountry()); //seller 来自的国家
        kafkaDTO.setSupply_origin(productDTO.getWriteUid()); // 上货来源：对应sourceType（预上货的）和 supply_origin（上架）
        // 商品标签的禁售禁运信息：
        spuTagInofBuilder(kafkaDTO, productDTO);
        log.info(" productid is {} ,productproperty is {} , feature is {}",
                productDTO.getProductId(), productDTO.getProductProperty(), JSONObject.toJSONString(productDTO.getFeatureMap()));
        kafkaDTO.setFeatrue(JSONUtil.object2String(productDTO.getFeatureMap())); // 商品feature属性
        kafkaDTO.setProductProperty(productDTO.getProductProperty()); // 商品属性
        kafkaDTO.setDescription(productDTO.getDescription()); // 图文详情
        kafkaDTO.setHas_description(BaseUtil.isNotEmpty(productDTO.getDescription())); // 图文详情标示
        kafkaDTO.setCurrency(productDTO.getCurrency());
        kafkaDTO.setSku_max_price(productDTO.getSkuMaxPrice());
        kafkaDTO.setSku_min_price(productDTO.getSkuMinPrice());
        log.info(" productid is {} ,description is {} ",
                productDTO.getProductId(), productDTO.getDescription());
        kafkaDTO.setStore_id(productDTO.getStoreId());
        return kafkaDTO;
    }

    private static void spuCategoryBulider(ProductTemplateRequestDTO kafkaDTO, ProductDTO productDTO) {
        if (BaseUtil.isEmpty(productDTO.getCategoryId())) {
            log.error("kafkaDTO init category null ");
            kafkaDTO.setCategory_id(Lists.newArrayList());   // 商品类目id（叶子类目）
        } else {
            kafkaDTO.setCategory_id(Lists.newArrayList(productDTO.getCategoryId()));   // 商品类目id（叶子类目）
        }
    }

    private static void spuFactory_idBuilder(ProductTemplateRequestDTO kafkaDTO, ProductDTO productDTO) {
        kafkaDTO.setFactory_id(BaseUtil.isEmpty(productDTO.getCompanyId()) ? null : productDTO.getCompanyId().toString());
    }

    /**
     * description: todo 价格实时化待改动点
     *
     * @author wyh
     * @date 2019/11/7 10:21
     */
    private static void spuList_priceBuilder(ProductTemplateRequestDTO kafkaDTO, ProductDTO productDTO) {
        kafkaDTO.setList_price(BaseUtil.isEmpty(productDTO.getListPrice()) ? null : productDTO.getListPrice().doubleValue());
        kafkaDTO.setMin_price(BaseUtil.isEmpty(productDTO.getMinPrice()) ? null : productDTO.getMinPrice().doubleValue());
    }

    private static void spuProduct_imagesBuilder(ProductTemplateRequestDTO kafkaDTO, ProductDTO productDTO) {
        if (BaseUtil.isEmpty(productDTO.getProductImages())) {
            log.error("productId is {} ,productNo is {}, 商品图片信息集合为空 ", productDTO.getProductId(), productDTO.getProductNo());
            kafkaDTO.setProduct_images(Lists.newArrayList());
            return;
        }

        //商品图片
        kafkaDTO.setProduct_images(productDTO.getProductImages()
                .stream()
                .filter(productImageDTO -> BaseUtil.isNotEmpty(productImageDTO))
                .sorted((image1, image2) -> {
                    if (BaseUtil.isEmpty(image1.getSequence()) || BaseUtil.isEmpty(image2.getSequence())) {
                        return 1;
                    } else {
                        return ~image1.getSequence().compareTo(image2.getSequence());
                    }
                })
                .map(image -> new ProductImagesRequestDTO(image.getUrl(), image.getUrlMiddle(), image.getUrlSmall()))
                .collect(Collectors.toList()));
    }

//    /**
//     * description: todo 价格是实化带改动点
//     *
//     * @author wyh
//     * @date 2019/11/7 10:21
//     */
//    private static void spuNew_list_priceAndMarking_priceBuilder(ProductTemplateRequestDTO kafkaDTO,
//                                                                 ProductDTO productDTO,
//                                                                 List<com.clubfactory.product.center.client.dto.product.SkuDTO> skuDTOS,
//                                                                 Map<String, Double> exchangeRates) {
//        Boolean useNewStructure = true;
//        if (BaseUtil.isNotEmpty(skuDTOS) && skuDTOS.size() > 100) {
//            log.warn("sku大于100个时候，不做计算多少划线价和多国兜底价 ；skuDTOS data size is  ", skuDTOS.size());
//            useNewStructure = false;
//        }
//        String newListPrice = null;
//        if (productDTO.getListPrice() != null && kafkaDTO.getList_price() != null) {
//            List<Integer> skuIds =
//                    skuDTOS.stream().map(com.clubfactory.product.center.client.dto.product.SkuDTO::getSkuId).collect(Collectors.toList());
//            if (BaseUtil.isEmpty(skuIds)) {
//                newListPrice = PriceUtil.getMultiCountryPrices(new ArrayList<>(), kafkaDTO.getList_price(), exchangeRates).toJSONString();
//            } else {
//                Map<String, Double> multiCountryRates = exchangeRates;
//                log.info("汇率 existedCountries , multiCountryRates {} ", JSON.toJSONString(multiCountryRates));
//                newListPrice = PriceUtil.getMultiCountryPrices(skuIds, kafkaDTO.getList_price(), multiCountryRates).toJSONString();
//
//            }
//        }
//        String markingPrice = null;
//        if (productDTO.getCPlatformPrice() != null) {
//            markingPrice = PriceUtil.getMultiCountryPrices(productDTO.getCPlatformPrice(), exchangeRates).toJSONString();
//        }
//        kafkaDTO.setNew_list_price(useNewStructure ? newListPrice : "");
//        kafkaDTO.setMarking_price(useNewStructure ? markingPrice : "");
//    }

    private static Boolean skuActiveBuilder(com.clubfactory.product.center.client.dto.product.SkuDTO skuDTO, ProductDTO productDTO) {

        if (BaseUtil.isNotEmpty(skuDTO.getActive()) && BaseUtil.isNotEmpty(ProductActiveEnum.getValue(skuDTO.getActive()))) {
            return ProductActiveEnum.getValue(skuDTO.getActive());
        } else {
            log.error("convertKafkaDTO#skuActiveBuilder error  :active is {}, productId is {},productNo is {}"
                    , productDTO.getActive(), productDTO.getProductId(), productDTO.getProductNo());
            return null;
        }
    }

    private static void spuActiveBuilder(ProductTemplateRequestDTO kafkaDTO, ProductDTO productDTO) {

        if (BaseUtil.isNotEmpty(productDTO.getActive()) && BaseUtil.isNotEmpty(ProductActiveEnum.getValue(productDTO.getActive()))) {
            kafkaDTO.setActive(ProductActiveEnum.getValue(productDTO.getActive()));
        } else {
            log.error("convertKafkaDTO#spuActiveBuilder  error  :active is {}, productId is {},productNo is {}"
                    , productDTO.getActive(), productDTO.getProductId(), productDTO.getProductNo());
            kafkaDTO.setActive(null);
        }
    }

    private static void spuAllThirdCategoryBulider(ProductTemplateRequestDTO kafkaDTO, List<CategoryDTO> multiLevelCategory) {
        List<Integer> allThirdCategoryIdList = Lists.newArrayList();
        if (BaseUtil.isNotEmpty(multiLevelCategory)) {
            CategoryDTO thirdCategory = multiLevelCategory.get(0);
            if (BaseUtil.isNotEmpty(thirdCategory)) {
                allThirdCategoryIdList.add(thirdCategory.getId());
                CategoryDTO secondCategory = thirdCategory.getParentCategory();
                if (BaseUtil.isNotEmpty(secondCategory)) {
                    allThirdCategoryIdList.add(secondCategory.getId());
                    CategoryDTO firstCategory = secondCategory.getParentCategory();
                    if (BaseUtil.isNotEmpty(firstCategory)) {
                        allThirdCategoryIdList.add(firstCategory.getId());
                    }
                }
            }
        }
        // 按照子-父的关系的顺序 向上直到一级类目；保存数据
        kafkaDTO.setAll_category_ids(allThirdCategoryIdList);
    }

    private static void spuElectricBuilder(ProductTemplateRequestDTO kafkaDTO, ProductDTO productDTO) {
        if (BaseUtil.isNotEmpty(productDTO.getElectric()) && BaseUtil.isNotEmpty(ProductElectricEnum.getValue(productDTO.getElectric()))) {
            kafkaDTO.setElectric(ProductElectricEnum.getValue(productDTO.getElectric()));
        } else {
            log.error("convertKafkaDTO#spuElectricBuilder error :electric is {}, productId is {} ,productNo is {}"
                    , productDTO.getElectric(), productDTO.getProductId(), productDTO.getProductNo());
            kafkaDTO.setElectric(null);
        }
    }


    private static void skuBuilder(
            ProductTemplateRequestDTO kafkaDTO,
            ProductDTO productDTO,
            List<SkuDTO> skuDTOS,
            Map<String, Double> exchangeRates) {

        log.info("skuBuilder skuDtos size is {} ,skuSttr size is {} ", skuDTOS.size());
        //  sku信息
        List<com.clubfactory.center.dto.kafka.product.SkuDTO> products = Lists.newArrayList();
        skuDTOS.forEach(sku -> {
            // sku属性
            List<SkuAttrSkuAttrValueDTO> skuAttrs = Lists.newArrayList();
            List<SkuAttributeDTO> attributes = sku.getAttributes();
            if (BaseUtil.isEmpty(attributes)) {
                attributes = Lists.newArrayList();
            }
            attributes.stream()
                    .filter(attr -> BaseUtil.isNotEmpty(attr))
                    .collect(Collectors.toList())
                    .forEach(x -> {
                        skuAttrs.add(new SkuAttrSkuAttrValueDTO(x.getAttributeKeyId(), x.getAttributeKeyName()
                                , x.getAttributeValueId(), x.getAttributeValueName(), x.getExt()));
                    });
            //todo 删除注释代码
//            String skuMarkingPrice = kafkaDTO.getMarking_price();
//            if (BaseUtil.isPositive(sku.getMarkingPrice())) {
//                skuMarkingPrice = JSONUtil.object2String(PriceUtil.getSkuMultiCountryPrices(sku.getMarkingPrice().doubleValue(), exchangeRates));
//            }

            products.add(new com.clubfactory.center.dto.kafka.product.SkuDTO()
                    .setId(sku.getSkuId())
                    .setActive(skuActiveBuilder(sku, productDTO))
                    .setSku(skuAttrs)
                    .setUpdateTime(sku.getUpdateTime())
                    .setImage_url(sku.getImageUrl())
//                    .setMarking_price(skuMarkingPrice)
                    .setCurrency(sku.getCurrency())
                    .setSku_marking_price(sku.getSkuMarkingPrice())
                    .setFeature(JSONUtil.object2String(BaseUtil.isNotEmpty(sku.getFeatureMap()) ? sku.getFeatureMap() : Maps.newHashMap())));
        });
        kafkaDTO.setProducts(products);
    }

    private static void spuTagInofBuilder(ProductTemplateRequestDTO kafkaDTO, ProductDTO productDTO) {
        FbNationsInfoOfProductSyncDTO syncDTO = new FbNationsInfoOfProductSyncDTO();
        List<String> tagFbNations = JSONObject.parseObject(productDTO.getTagFbNations(), new TypeReference<List<String>>() {
        });
        List<String> tagFbNationsList = BaseUtil.isNotEmpty(tagFbNations) ? tagFbNations : Lists.newArrayList();
        syncDTO.setForbidden_sale_nations(tagFbNationsList);
        Map<String, List<String>> stringListMap = JSONObject.parseObject(productDTO.getTagTrNations(), new TypeReference<Map<String, List<String>>>() {
        });
        HashMap<String, List<String>> emptyMap = Maps.newHashMap();
        emptyMap.put("ts", Lists.newArrayList());
        emptyMap.put("jy", Lists.newArrayList());
        emptyMap.put("pt", Lists.newArrayList());
        Map<String, List<String>> map = BaseUtil.isNotEmpty(stringListMap) ? stringListMap : emptyMap;
        syncDTO.setForbidden_transport_nations(map);
        syncDTO.setForbidden_tag_ids(parseForbiddenTagId(productDTO.getProductId(), productDTO.getIllegalTags()));
        syncDTO.setPid(productDTO.getProductId());
        kafkaDTO.setForbidden_nations_info(syncDTO);
    }

    private static Set<Integer> parseForbiddenTagId(Integer pid, String illegalTag) {
        try {
            if (StringUtils.isNotEmpty(illegalTag)) {
                return Stream.of(illegalTag.split(",")).map(Integer::valueOf).collect(Collectors.toSet());
            }
        } catch (Exception e) {
            log.error("parse forbidden tag id fail,pid:{},illegalTag:{},e:{}", pid, illegalTag, e.getMessage());
        }
        return Sets.newHashSet();
    }

    public static boolean containNotAvailableImage(ProductTemplateRequestDTO kakfaDto) {
        Set<String> imageSet = Sets.newHashSet();
        //主图
        imageSet.add(kakfaDto.getImage_url());
        //轮播图
        if (BaseUtil.isNotEmpty(kakfaDto.getProduct_images())) {
            imageSet.addAll(kakfaDto.getProduct_images().stream().map(x -> x.getUrl()).collect(Collectors.toList()));
        }
        //sku图片
        if (BaseUtil.isNotEmpty(kakfaDto.getProducts().stream().map(x -> x.getImage_url()).collect(Collectors.toList()))) {
            imageSet.addAll(kakfaDto.getProducts().stream().map(x -> x.getImage_url()).collect(Collectors.toList()));
        }

        //过滤不是s3的图片
        imageSet = imageSet.stream()
                .filter(url -> BaseUtil.isNotEmpty(url))
                .filter(url -> !url.startsWith(s3Url))
                .collect(Collectors.toSet());
        if (BaseUtil.isNotEmpty(imageSet)) {
            log.warn("商品中包含未转换图片, 不进行同步 productTemplateId:{}, imageList:{}", kakfaDto.getId(), imageSet);
            return true;
        }
        return false;
    }


}
