package com.clubfactory.center.oldDto;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 为了兼容老格式
 * <p>
 * Created by guotie on 2019/3/11.
 */
@Data
@Accessors(chain = true)
public class SkuCountryPriceDTO implements Serializable {

    private static final long serialVersionUID = -2943722870241399649L;

    private Integer skuId;

    @JSONField(name = "default")
    private Double defaultPrice;

    private Object localPrice;


    public SkuCountryPriceDTO(Integer skuId, Double defaultPrice, Object localPrice) {
        this.skuId = skuId;
        this.defaultPrice = defaultPrice;
        this.localPrice = localPrice;
    }

    public SkuCountryPriceDTO() {
    }
}
