package com.clubfactory.center.oldDto;

import com.alibaba.fastjson.JSONObject;
import lombok.Data;

import java.io.Serializable;

/**
 * description:
 *
 * @author wyh
 * @date 2019/11/11 11:38
 */
@Data
public class ProductPriceDTO  implements Serializable {
    private Integer id;

    private Boolean overwrite = false;

    private String productNo;

    private JSONObject multi_country_prices;
}
