package com.clubfactory.center.dto.mns;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;

/**
 * description:
 * @author wyh
 * @date 2019/11/1 14:39
 */
@Data
@Accessors(chain = true)
public class MnsMessageDTO implements Serializable{
    private List<MnsMessageItemDTO> products;

    public MnsMessageDTO(List<MnsMessageItemDTO> products) {
        this.products = products;
    }

    public MnsMessageDTO() {
    }
}
