package com.clubfactory.center.dto.mns;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@Accessors(chain = true)
public class MnsMessageItemDTO implements Serializable{
    private String id;
    private String type;

    public MnsMessageItemDTO(String id, String type) {
        this.id = id;
        this.type = type;
    }

    public MnsMessageItemDTO() {
    }
}
