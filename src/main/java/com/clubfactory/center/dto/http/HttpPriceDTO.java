package com.clubfactory.center.dto.http;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;
/**
 * description: http价格消息格式
 *
 *  product_template_id_list格式:   2106263:EMP002106263N
 *
 * @author wyh
 * @date 2019/11/1 14:28
 */

@Data
@Accessors(chain = true)
public class HttpPriceDTO {
    private List<String> product_template_id_list;

    public HttpPriceDTO(List<String> product_template_id_list) {
        this.product_template_id_list = product_template_id_list;
    }

    public HttpPriceDTO() {
    }
}
