package com.clubfactory.center.dto.http;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * http商品消息格式
 * Created by minghua.zmh on 2018/6/11.
 */
@Data
@Accessors(chain = true)
public class HttpProductDTO {
    private String action;
    private List<Integer> product_template_id_list;

    public HttpProductDTO(List<Integer> product_template_id_list, String action) {
        this.action = action;
        this.product_template_id_list = product_template_id_list;
    }

    public HttpProductDTO() {
    }
}
