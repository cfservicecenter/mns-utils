package com.clubfactory.center.dto.kafka.price;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
public class KafkaSkuPriceDTO implements Serializable{
    private Integer skuId;
    private BigDecimal price;
    private BigDecimal skuMarkingPrice;
}
