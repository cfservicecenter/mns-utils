package com.clubfactory.center.dto.kafka.product;

import lombok.Data;

import java.io.Serializable;

@Data
public class ProductImagesRequestDTO implements Serializable {

    private String url;

    private String url_small;

    private String url_middle;

    public ProductImagesRequestDTO(String url, String url_small, String url_middle) {
        this.url = url;
        this.url_small = url_small;
        this.url_middle = url_middle;
    }
}
