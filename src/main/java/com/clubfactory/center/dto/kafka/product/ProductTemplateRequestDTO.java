package com.clubfactory.center.dto.kafka.product;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * @author: zhh
 * @create: 2018/6/27 2:09 PM
 * @version: 1.0
 */
@Data
public class ProductTemplateRequestDTO implements Serializable {

    private Integer id;

    private String product_no;

    private List<Integer> category_id;

    private List<Integer> all_category_ids;

    private Boolean active;

    /**
     *  标题
     */
    private String name;

    private String image_url;

    /**
     *  销量
     */
    private Integer orders;

    @Deprecated
    private Double list_price;
    /**
     * 新的兜底价格，包含多国价格
     */
    @Deprecated
    private String new_list_price;

    private List<ProductImagesRequestDTO> product_images;

    private List<SkuDTO> products;
    /**
     *  不知道干什么的
     */
    private String factory_id;

    private Boolean electric;
    /**
     *  不知
     */
    private String b_average_rating;
    /**
     *  规格
     */
    private String b_specifics;
    /**
     *  不知
     */
    private String overview;

    @Deprecated
    private Double c_platform_price;
    /**
     * 划线价，包含多国价格
     */
    @Deprecated
    private String marking_price;
    /**
     *  不知
     */
    private String c_platform_name;
    /**
     *  不知
     */
    private String c_details;
    /**
     *  不知
     */
    @Deprecated
    private Double benchmark_price;
    /**
     *  测试标志
     */
    private Integer daily_testing_flag;
    /**
     *  搜索关键词
     */
    private String search_keywords;

    private Long created_time;

    private Long updated_time;

    private String illegal_tags;

    // 禁售禁运信息
    private FbNationsInfoOfProductSyncDTO forbidden_nations_info;
    /**
     *  供应源
     */
    private Integer supply_origin;

    private Integer biz_model;

    private Integer source_id;

    private Integer seller_id;

    private String seller_name;

    private String seller_country;

    /**
     * 商品属性字段
     */
    private String  productProperty;

    /**
     * 业务标签 json格式的kv对
     */
    private String  featrue;

    /**
     * Description 图文详情
     */
    private String  description;

    /**
     * Description 是否有图文详情
     */
    private Boolean  has_description;

    /**
     *  sku最低价
     */
    @Deprecated
    private Double min_price;

    /****************所有价格最终保留这三个******************/
    /**
     * @Fields currency 币种
     */
    private String currency;

    /**
     * sku最高价
     */
    private BigDecimal sku_max_price;
    /**
     * sku最低价
     */
    private BigDecimal sku_min_price;
    /****************所有价格最终保留这三个******************/

    private Integer store_id;
}
