package com.clubfactory.center.dto.kafka.product;

import lombok.Data;

@Data
public class DeleteProductDTO {
    private Integer id;

    private Long deleted_time;

    private String productNo;

    public DeleteProductDTO(Integer id, Long deleted_time, String productNo) {
        this.id = id;
        this.deleted_time = deleted_time;
        this.productNo = productNo;
    }
}
