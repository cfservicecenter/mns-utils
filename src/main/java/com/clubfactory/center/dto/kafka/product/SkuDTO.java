package com.clubfactory.center.dto.kafka.product;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Data
@Accessors(chain = true)
public class SkuDTO implements Serializable {

    private Integer id;

    private Boolean active;

    private Date updateTime;
    /**
     *  sku图片
     */
    private String image_url;
    /**
     * 划线价，包含多国价格
     */
    @Deprecated
    private String marking_price;

    /**
     *  json格式的kv对
     */
    private String  feature;

    /****************所有价格最终保留这两个个******************/
    /**
     * @Fields currency 币种
     */
    private String currency;


    /**
     * 带币种划线价
     */
    private BigDecimal sku_marking_price;
    /****************所有价格最终保留这两个个******************/

    private List<SkuAttrSkuAttrValueDTO> sku;

    public SkuDTO(Integer id, Boolean active, List<SkuAttrSkuAttrValueDTO> sku) {
        this.id = id;
        this.active = active;
        this.sku = sku;
    }

    public SkuDTO(Integer id, Boolean active, List<SkuAttrSkuAttrValueDTO> sku, Date updateTime) {
        this.id = id;
        this.active = active;
        this.sku = sku;
        this.updateTime = updateTime;
    }

    public SkuDTO(Integer id,
                  Boolean active,
                  List<SkuAttrSkuAttrValueDTO> sku,
                  Date updateTime,
                  String image_url,
                  String marking_price) {
        this.id = id;
        this.active = active;
        this.sku = sku;
        this.updateTime = updateTime;
        this.image_url = image_url;
        this.marking_price = marking_price;
    }

    public SkuDTO() {
    }
}
