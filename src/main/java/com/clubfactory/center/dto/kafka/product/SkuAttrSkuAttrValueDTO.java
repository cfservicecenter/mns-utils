package com.clubfactory.center.dto.kafka.product;

import lombok.Data;

import java.io.Serializable;

@Data
public class SkuAttrSkuAttrValueDTO implements Serializable {
    private Integer attr_id;

    private String attr_name;

    private Integer attr_value_id;

    private String attr_value_name;
    /**
     * 属性值扩展字段，对于size属性 存放尺码转换信息
     */
    private String attr_value_ext;

    public SkuAttrSkuAttrValueDTO(Integer attr_id,
                                  String attr_name,
                                  Integer attr_value_id,
                                  String attr_value_name,
                                  String attr_value_ext) {
        this.attr_id = attr_id;
        this.attr_name = attr_name;
        this.attr_value_id = attr_value_id;
        this.attr_value_name = attr_value_name;
        this.attr_value_ext = attr_value_ext;
    }
}
