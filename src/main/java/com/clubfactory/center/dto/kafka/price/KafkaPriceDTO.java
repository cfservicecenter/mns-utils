package com.clubfactory.center.dto.kafka.price;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * description:
 * @author wyh
 * @date 2019/11/1 14:39
 */
@Data
@Accessors(chain = true)
public class KafkaPriceDTO implements Serializable{

    /**
     * 商品id
     */
    private Integer id;

    private Boolean overwrite = false;

    /**
     * 货号
     */
    private String productNo;

    /**
     * 币种
     */
    private String currency;

    /**
     * 多国价格
     */
    private Map<String, List<KafkaSkuPriceDTO>> multiCountryPrices;

    private Long updateTime;
}

