package com.clubfactory.center.dto.kafka.product;

import lombok.Data;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Data
public class FbNationsInfoOfProductSyncDTO implements Serializable {
    private Integer pid; //商品id
    private List<String> forbidden_sale_nations; //禁售国家
    private Map<String,List<String>> forbidden_transport_nations; //禁运国家
    private Set<Integer> forbidden_tag_ids;//标签
}
