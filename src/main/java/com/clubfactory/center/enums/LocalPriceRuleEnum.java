package com.clubfactory.center.enums;

/**
 * Created by guotie on 2019/3/11.
 */
public enum LocalPriceRuleEnum {

    ROUND_TO_NUMBER("四舍五入取整"),
    ROUND_UP_TO_NUMBER("四舍五入向上取整"),
    ROUND_TO_DECIMAL("四舍五入保留两位小数");

    private String name;

    LocalPriceRuleEnum(String name){
        this.name = name;
    }
}
