package com.clubfactory.center.enums;

/**
 * @ClassName:ProductElectricEnum
 * @Description: Spring boot 规约大于配置
 * @Author kof.wang
 * @Date : 2019-07-05 19:52
 * @Version 1.0.0
 **/
public enum  ProductElectricEnum {

    ELETRIC_TRUE(0, false),
    ELETRIC_FALSE(1,true );


    Integer key;
    Boolean value;


    ProductElectricEnum(Integer key, Boolean value) {
        this.key = key;
        this.value = value;
    }

    public static  Boolean getValue(Integer key) {
        if (ELETRIC_FALSE.key.intValue()==key){
            return ELETRIC_FALSE.value;
        }else if (ELETRIC_TRUE.key.intValue()==key){
            return ELETRIC_TRUE.value;
        }else {
            return null;
        }
    }
}
