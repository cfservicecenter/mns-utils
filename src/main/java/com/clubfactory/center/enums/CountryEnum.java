package com.clubfactory.center.enums;

/**
 * @Description: 国家枚举
 * @Author: wyh
 * @Date: 2018/7/19 10:51
 */
public enum CountryEnum {

    INDIA("india","India"),
    INDIA_LOCAL("indiaLocal","India Local"),
    MEXICO("mexico","Mexico"),
    UNITED_KINGDOM("uk","United Kingdom"),
    UNITED_STATES("us","United States"),
    QATAR("qatar","Qatar"),
    SAUDI_ARABIA("sa","Saudi Arabia"),
    UNITED_ARAB_EMIRATES("uae","United Arab Emirates"),
    INDONESIA("id","Indonesia"),
    OTHER("other","Other");
    /**
     * 名称
     */
    public final String key;
    public final String value;

    CountryEnum(String key, String value) {
        this.key=key;
        this.value = value;
    }
}
