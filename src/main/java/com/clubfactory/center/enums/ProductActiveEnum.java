package com.clubfactory.center.enums;

/**
 * @ClassName:ProductActiveEnum
 * @Description: Spring boot 规约大于配置
 * @Author kof.wang
 * @Date : 2019-07-05 11:19
 * @Version 1.0.0
 **/
public enum  ProductActiveEnum {

    ACTIVE_ONLINE(0, false),
    ACTIVE_OFFLINE(1,true );
    

    Integer key;
    Boolean value;


    ProductActiveEnum(Integer key, Boolean value) {
        this.key = key;
        this.value = value;
    }

    public static  Boolean getValue(Integer key) {
        if (ACTIVE_OFFLINE.key.intValue()==key){
            return ACTIVE_OFFLINE.value;
        }else if (ACTIVE_ONLINE.key.intValue()==key){
            return ACTIVE_ONLINE.value;
        }else {
            return null;
        }
    }
}
