package com.clubfactory.center.enums;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by guotie on 2019/3/11.
 */
public enum CountryLocalPriceRuleEnum {

    AUSTRALIA("Australia",                          "au", "AUD",           "AUD",       LocalPriceRuleEnum.ROUND_TO_DECIMAL, "澳大利亚"),
    BAHRAIN("Bahrain",                              "bh", "BHD",           "BHD",       LocalPriceRuleEnum.ROUND_TO_DECIMAL, "巴林"),
    BANGLADESH("Bangladesh",                        "bd", "BDT",           "BDT",       LocalPriceRuleEnum.ROUND_TO_DECIMAL, "孟加拉共和国"),
    BRAZIL("Brazil",                                "br", "BRL",           "R$",        LocalPriceRuleEnum.ROUND_TO_DECIMAL,  "巴西"),
    CANADA("Canada",                                "ca", "CAD",           "CAD",       LocalPriceRuleEnum.ROUND_TO_DECIMAL, "加拿大"),
    EGYPT("Egypt",                                  "eg", "EGP",           "EGP",       LocalPriceRuleEnum.ROUND_TO_DECIMAL, "埃及"),
    FRANCE("France",                                "fr", "EUR",           "EUR",       LocalPriceRuleEnum.ROUND_TO_DECIMAL, "法国"),
    INDIA("India",                                  "in", "INR",           "₹",         LocalPriceRuleEnum.ROUND_TO_NUMBER, "印度"),
    INDONESIA("Indonesia",                          "id", "IDR",           "IDR",       LocalPriceRuleEnum.ROUND_TO_NUMBER, "印度尼西亚"),
    ISRAEL("Israel",                                "il", "ILS",           "ILS",       LocalPriceRuleEnum.ROUND_TO_DECIMAL, "以色列"),
    ITALY("Italy",                                  "it", "EUR",           "EUR",       LocalPriceRuleEnum.ROUND_TO_DECIMAL, "意大利"),
    KUWAIT("Kuwait",                                "kw", "KWD",           "KWD",       LocalPriceRuleEnum.ROUND_TO_DECIMAL, "科威特"),
    LEBANON("Lebanon",                              "lb", "LBP",           "LBP",       LocalPriceRuleEnum.ROUND_TO_DECIMAL, "黎巴嫩"),
    MALAYSIA("Malaysia",                            "my", "MYR",           "MYR",       LocalPriceRuleEnum.ROUND_TO_DECIMAL, "马来西亚"),
    MEXICO("Mexico",                                "mx", "MXN",           "MXN",       LocalPriceRuleEnum.ROUND_TO_DECIMAL, "墨西哥"),
    OMAN("Oman",                                    "om", "OMR",           "OMR",       LocalPriceRuleEnum.ROUND_TO_DECIMAL, "阿曼"),
    PHILIPPINES("Philippines",                      "ph", "PHP",           "PHP",       LocalPriceRuleEnum.ROUND_TO_DECIMAL, "菲律宾"),
    QATAR("Qatar",                                  "qa", "QAR",           "QAR",       LocalPriceRuleEnum.ROUND_TO_DECIMAL, "卡塔尔"),
    SAUDI_ARABIA("Saudi Arabia",                    "sa", "SAR",           "SAR",       LocalPriceRuleEnum.ROUND_TO_DECIMAL, "沙特阿拉伯"),
    SINGAPORE("Singapore",                          "sg", "SGD",           "SGD",       LocalPriceRuleEnum.ROUND_TO_DECIMAL, "新加坡"),
    SPAIN("Spain",                                  "es", "EUR",           "EUR",       LocalPriceRuleEnum.ROUND_TO_DECIMAL, "西班牙"),
    SRI_LANKA("Sri Lanka",                          "lk", "LKR",           "LKR",       LocalPriceRuleEnum.ROUND_TO_DECIMAL, "斯里兰卡"),
    THAILAND("Thailand",                            "th", "THB",           "THB",       LocalPriceRuleEnum.ROUND_TO_DECIMAL, "泰国"),
    UNITED_ARAB_EMIRATES("United Arab Emirates",    "ae", "AED",           "AED",       LocalPriceRuleEnum.ROUND_TO_DECIMAL, "阿联酋"),
    UNITED_KINGDOM("United Kingdom",                "gb", "GBP",           "GBP",       LocalPriceRuleEnum.ROUND_TO_DECIMAL, "英国"),
    UNITED_STATES("United States",                  "us", "USD",           "USD",       LocalPriceRuleEnum.ROUND_TO_DECIMAL, "美国"),
    VIETNAM("Vietnam",                              "vn", "VND",           "VND",       LocalPriceRuleEnum.ROUND_TO_NUMBER, "越南"),
    OTHER("Other",                              "others", "USD",               null,          LocalPriceRuleEnum.ROUND_TO_NUMBER, "其他");


    private String country;

    private String country4Short;

    private String country4Exchange;

    private String country4Cn;

    private String sign;

    private LocalPriceRuleEnum localPriceRuleEnum;

    public String getCountry() {
        return country;
    }

    public String getCountry4Short() {
        return country4Short;
    }

    public String getCountry4Exchange() {
        return country4Exchange;
    }

    public String getCountry4Cn() {
        return country4Cn;
    }

    public String getSign() {
        return sign;
    }

    public LocalPriceRuleEnum getLocalPriceRuleEnum() {
        return localPriceRuleEnum;
    }

    CountryLocalPriceRuleEnum(String country, String country4Short, String country4Exchange, String sign, LocalPriceRuleEnum localPriceRuleEnum, String country4Cn){
        this.country = country;
        this.country4Short = country4Short;
        this.country4Exchange = country4Exchange;
        this.country4Cn = country4Cn;
        this.sign = sign;
        this.localPriceRuleEnum = localPriceRuleEnum;
    }

    public static final Map<String, CountryLocalPriceRuleEnum> lookup = new HashMap();
    public static final Map<String, CountryLocalPriceRuleEnum> country4ShortMap = new HashMap();


    static {
        for (CountryLocalPriceRuleEnum country : EnumSet.allOf(CountryLocalPriceRuleEnum.class)) {
            lookup.put(country.getCountry(), country);
            country4ShortMap.put(country.getCountry4Short(), country);
        }
    }

    public static CountryLocalPriceRuleEnum find(String country) {
        return lookup.get(country);
    }

    public static CountryLocalPriceRuleEnum findByCountry4Short(String country4Shor) {
        return country4ShortMap.get(country4Shor);
    }
}
