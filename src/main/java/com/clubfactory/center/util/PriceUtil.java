package com.clubfactory.center.util;

import com.alibaba.fastjson.JSONObject;
import com.clubfactory.center.common.util.MathUtil;
import com.clubfactory.center.oldDto.SkuCountryPriceDTO;
import com.clubfactory.center.enums.CountryEnum;
import com.clubfactory.center.enums.CountryLocalPriceRuleEnum;
import com.clubfactory.center.enums.LocalPriceRuleEnum;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.clubfactory.center.enums.CountryEnum.INDIA;


/**
 * Created by guotie on 2019/3/11.
 */
public class PriceUtil {


    public static final String DEFAULT = "default";
    public static final String LOCAL_PRICE = "localPrice";
    public static final String LOCAL_PRICE_SIGN = "localPriceSign";
    public static final String SKU_LIST = "skuList";
    public static final String OTHERS = "others";

    /**
     * 计算一个商品多国价格
     *
     * @param productProductIds
     * @param defaultPrice
     * @param multiCountryRates 汇率
     * @return
     */
    public static JSONObject getMultiCountryPrices(List<Integer> productProductIds, Double defaultPrice, Map<String, Double> multiCountryRates) {
        JSONObject multiCountryPrice = new JSONObject();
        CountryEnum[] values = CountryEnum.values();
        for (CountryEnum countryEnum : values) {
            if (countryEnum.equals(CountryEnum.INDIA_LOCAL)) {
                continue;
            }
            String country = countryEnum.value;
            if (!country.equals("Other")) {
                JSONObject priceJson = new JSONObject();
                priceJson.put(DEFAULT, defaultPrice);
                priceJson.put(LOCAL_PRICE, getLocalPrice(defaultPrice, country, multiCountryRates));
                priceJson.put(LOCAL_PRICE_SIGN, CountryLocalPriceRuleEnum.find(country).getSign());
                priceJson.put(SKU_LIST, initSkuDTOs(productProductIds, defaultPrice, getLocalPrice(defaultPrice, country, multiCountryRates)));
                multiCountryPrice.put(CountryLocalPriceRuleEnum.find(country).getCountry4Short(), priceJson);
            }
        }

        JSONObject priceJson = new JSONObject();
        priceJson.put(DEFAULT, defaultPrice);
        priceJson.put(SKU_LIST, initSkuDTOs(productProductIds, defaultPrice, null));
        multiCountryPrice.put(OTHERS, priceJson);
        return multiCountryPrice;
    }

    /**
     * 计算一个商品多国价格
     *
     * @param productProductIds
     * @param existedCountries  系统已存在的多国的基础价格
     * @param multiCountryRates
     * @return
     */
    public static JSONObject getMultiCountryPrices(List<Integer> productProductIds, Map<String, Double> existedCountries, Map<String, Double> multiCountryRates) {
        JSONObject multiCountryPrice = new JSONObject();

        existedCountries.forEach((country, defaultPrice) -> {
            if (!country.equals("Other")) {
                JSONObject priceJson = new JSONObject();
                priceJson.put(DEFAULT, defaultPrice);
                priceJson.put(LOCAL_PRICE, getLocalPrice(defaultPrice, country, multiCountryRates));
                priceJson.put(LOCAL_PRICE_SIGN, CountryLocalPriceRuleEnum.find(country).getSign());
                priceJson.put(SKU_LIST, initSkuDTOs(productProductIds, defaultPrice, getLocalPrice(defaultPrice, country, multiCountryRates)));
                multiCountryPrice.put(CountryLocalPriceRuleEnum.find(country).getCountry4Short(), priceJson);
            }
        });
        Double defaultPrice = existedCountries.get("Other");
        if (defaultPrice != null) {
            JSONObject priceJson = new JSONObject();
            priceJson.put(DEFAULT, defaultPrice);
            priceJson.put(SKU_LIST, initSkuDTOs(productProductIds, defaultPrice, null));
            multiCountryPrice.put(OTHERS, priceJson);
        }
        return multiCountryPrice;
    }

    public static JSONObject getSkuMultiCountryPrices(Double defaultPrice, Map<String, Double> multiCountryRates) {
        if (defaultPrice == null) {
            return null;
        }
        JSONObject multiCountryPrice = new JSONObject();
        for (CountryEnum country : CountryEnum.values()) {
            if (country.equals(CountryEnum.INDIA_LOCAL)) {
                continue;
            }
            switch (country) {
                case OTHER: {
                    JSONObject priceJson = new JSONObject();
                    priceJson.put(DEFAULT, defaultPrice);
                    multiCountryPrice.put(OTHERS, priceJson);
                    break;
                }
                default: {
                    JSONObject priceJson = new JSONObject();
                    priceJson.put(DEFAULT, defaultPrice);
                    priceJson.put(LOCAL_PRICE, getLocalPrice(defaultPrice, country.value, multiCountryRates));
                    priceJson.put(LOCAL_PRICE_SIGN, CountryLocalPriceRuleEnum.find(country.value).getSign());
                    multiCountryPrice.put(CountryLocalPriceRuleEnum.find(country.value).getCountry4Short(), priceJson);
                    break;
                }
            }
        }
        return multiCountryPrice;
    }


    public static BigDecimal getSkuIndiaPrice(JSONObject multiCountryPrice) {
        if (multiCountryPrice == null) {
            return null;
        }
        if (multiCountryPrice.containsKey(INDIA.value)) {
            JSONObject indiaPrice = multiCountryPrice.getJSONObject(INDIA.value);
            return indiaPrice.getBigDecimal(DEFAULT);
        }
        return null;
    }

    /**
     * 计算一个SKU多国价格
     *
     * @param defaultPrice
     * @param multiCountryRates
     * @return
     */
    public static JSONObject getMultiCountryPrices(Double defaultPrice, Map<String, Double> multiCountryRates) {

        JSONObject multiCountryPrice = new JSONObject();

        CountryEnum[] values = CountryEnum.values();
        for (CountryEnum countryEnum : values) {
            String country = countryEnum.value;
            if (countryEnum.equals(CountryEnum.INDIA_LOCAL)) {
                continue;
            }
            if (!country.equals("Other")) {
                JSONObject priceJson = new JSONObject();
                priceJson.put(DEFAULT, defaultPrice);
                priceJson.put(LOCAL_PRICE, getLocalPrice(defaultPrice, country, multiCountryRates));
                priceJson.put(LOCAL_PRICE_SIGN, CountryLocalPriceRuleEnum.find(country).getSign());
                multiCountryPrice.put(CountryLocalPriceRuleEnum.find(country).getCountry4Short(), priceJson);
            }
        }

        JSONObject priceJson = new JSONObject();
        priceJson.put(DEFAULT, defaultPrice);
        multiCountryPrice.put(OTHERS, priceJson);
        return multiCountryPrice;
    }

    private static List<SkuCountryPriceDTO> initSkuDTOs(List<Integer> productProductIds, Double defaultPrice, Object localPrice) {
        if (productProductIds == null || productProductIds.size() == 0) {
            return new ArrayList<>();
        }
        return productProductIds.stream().map(skuId -> new SkuCountryPriceDTO(skuId, defaultPrice, localPrice)).collect(Collectors.toList());
    }

    private static Object getLocalPrice(Double defaultPrice, String country, Map<String, Double> multiCountryRates) {
        Double rate = multiCountryRates.get(CountryLocalPriceRuleEnum.find(country).getCountry4Exchange());
        if (rate == null) {
            rate = 1.0;
        }
        return getLocalPrice(defaultPrice, country, rate);
    }

    private static Object getLocalPrice(Double defaultPrice, String country, Double rate) {
        if (CountryLocalPriceRuleEnum.find(country).getLocalPriceRuleEnum().equals(LocalPriceRuleEnum.ROUND_TO_DECIMAL)) {
            return MathUtil.roundN(NumberUtil.mul(defaultPrice, rate),2);
        } else if (CountryLocalPriceRuleEnum.find(country).getLocalPriceRuleEnum().equals(LocalPriceRuleEnum.ROUND_TO_NUMBER)) {
            return MathUtil.roundN(NumberUtil.mul(defaultPrice, rate),0).intValue();
        } else {
            return (int)Math.ceil((NumberUtil.mul(defaultPrice, rate)));
        }
    }
}
