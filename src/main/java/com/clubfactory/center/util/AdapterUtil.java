package com.clubfactory.center.util;

import com.alibaba.fastjson.JSONObject;
import com.clubfactory.center.common.util.BaseUtil;
import com.clubfactory.center.common.util.CopyUtil;
import com.clubfactory.center.common.util.JSONUtil;
import com.clubfactory.center.common.util.MathUtil;
import com.clubfactory.center.dto.kafka.price.KafkaPriceDTO;
import com.clubfactory.center.dto.kafka.price.KafkaSkuPriceDTO;
import com.clubfactory.center.enums.CountryLocalPriceRuleEnum;
import com.clubfactory.center.enums.LocalPriceRuleEnum;
import com.clubfactory.center.oldDto.ProductPriceDTO;
import com.clubfactory.center.oldDto.SkuCountryPriceDTO;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * description: 新格式数据转换为老格式数据
 *
 * @author wyh
 * @date 2019/11/11 11:31
 */
@Slf4j
public class AdapterUtil {

    /**
     * description: 新格式价格转换为老格式价格
     *
     * @author wyh
     * @date 2019/11/12 16:51
     */
    public static ProductPriceDTO adaptPrice(KafkaPriceDTO kafkaPriceDTO, Map<String, Double> exchangeRates) {
        if (BaseUtil.isEmpty(kafkaPriceDTO)) {
            return null;
        }

        ProductPriceDTO productPriceDTO = CopyUtil.copyProperties(kafkaPriceDTO, ProductPriceDTO.class);
        JSONObject multiCountryPrices = getJsonMultiCountryPrices(kafkaPriceDTO.getMultiCountryPrices(), kafkaPriceDTO.getCurrency(), exchangeRates);
        productPriceDTO.setMulti_country_prices(multiCountryPrices);

        return productPriceDTO;
    }

    /**
     * description: 币种转换
     *
     * @author wyh
     * @date 2019/11/12 19:59
     */
    public static BigDecimal getPriceWithCountry(BigDecimal price, String currency, String countryCode, Map<String, Double> exchangeRates) {
        if (BaseUtil.isEmpty(price) || BaseUtil.isEmpty(currency) || BaseUtil.isEmpty(countryCode) || BaseUtil.isEmpty(exchangeRates)) {
            return price;
        }

        CountryLocalPriceRuleEnum countryLocalPriceRuleEnum = CountryLocalPriceRuleEnum.findByCountry4Short(countryCode);
        if (BaseUtil.isEmpty(countryLocalPriceRuleEnum)) {
            log.error("价格转换目标国家缺失, countryCode={} ", countryCode);
            return null;//只能null
        }

        Double currencyExchange = exchangeRates.get(currency);
        if (BaseUtil.isNotPositive(currencyExchange)) {
            log.error("价格转换币种汇率缺失, currency={}, exchangeRates", currency, JSONUtil.object2String(exchangeRates));
            return null;
        }

        Double targetCountryExchange = exchangeRates.get(countryLocalPriceRuleEnum.getCountry4Exchange());
        if (BaseUtil.isNotPositive(targetCountryExchange)) {
            log.error("价格转换目标汇率缺失, countryCode={}, exchangeRates", countryLocalPriceRuleEnum.getCountry4Exchange(), JSONUtil.object2String(exchangeRates));
            return null;
        }

        //price / currencyExchange 转换为美元    乘以targetCountryExchange转换为目标币种
        return new BigDecimal(
                getCountryFormatPrice(countryLocalPriceRuleEnum,
                        NumberUtil.mul(
                                NumberUtil.div(price.doubleValue(), currencyExchange),
                                targetCountryExchange)));
    }


    private static JSONObject getJsonMultiCountryPrices(
            Map<String, List<KafkaSkuPriceDTO>> multiCountryPrices,
            String currency,
            Map<String, Double> exchangeRates) {

        if (BaseUtil.isEmpty(multiCountryPrices)) {
            return new JSONObject();
        }
        if (BaseUtil.isEmpty(currency)) {
            log.error("币种不能为空");
            return new JSONObject();
        }
        if (BaseUtil.isEmpty(exchangeRates)) {
            log.error("汇率不能为空");
            return new JSONObject();
        }

        JSONObject multiCountryPrice = new JSONObject();
        multiCountryPrices.forEach((country, priceList) -> {
            if (BaseUtil.isEmpty(priceList)) {
                return;
            }
            CountryLocalPriceRuleEnum countryLocalPriceRuleEnum = CountryLocalPriceRuleEnum.findByCountry4Short(country);

            Double currencyExchange = exchangeRates.get(currency);
            if (BaseUtil.isNotPositive(currencyExchange)) {
                log.error("价格转换币种汇率缺失, currency={}, exchangeRates", currency, JSONUtil.object2String(exchangeRates));
                return;
            }
            BigDecimal defaultPrice4 = priceList.stream().map(KafkaSkuPriceDTO::getPrice).max(BigDecimal::compareTo).get();
            Double usdPrice = NumberUtil.div(defaultPrice4.doubleValue(), currencyExchange);
            JSONObject priceJson = new JSONObject();
            priceJson.put("default", MathUtil.roundN(usdPrice, 2));
            priceJson.put("localPrice", getLocalPriceNew(usdPrice, countryLocalPriceRuleEnum, exchangeRates));
            priceJson.put("localPriceSign", countryLocalPriceRuleEnum.getSign());

            //转换为美元
            priceList.stream().filter(item -> BaseUtil.isPositive(item.getPrice()))
                    .forEach(item -> item.setPrice(new BigDecimal(NumberUtil.div(item.getPrice().doubleValue(), currencyExchange))));
            priceJson.put("skuList", initSkuDTOsNew(priceList, countryLocalPriceRuleEnum, exchangeRates));
            multiCountryPrice.put(country, priceJson);

        });

        return multiCountryPrice;

    }


    /**
     * @param defaultPrice              1  美元价格
     * @param countryLocalPriceRuleEnum 2 国家
     * @param multiCountryRates         3 多国汇率集合
     * @return
     */
    private static Object getLocalPriceNew(Double defaultPrice, CountryLocalPriceRuleEnum countryLocalPriceRuleEnum, Map<String, Double> multiCountryRates) {
        if (BaseUtil.isEmpty(countryLocalPriceRuleEnum) || countryLocalPriceRuleEnum.equals(CountryLocalPriceRuleEnum.OTHER)) {
            return null;
        }
        String exchangeKey = countryLocalPriceRuleEnum.getCountry4Exchange();
        Double rate = multiCountryRates.containsKey(exchangeKey) ? multiCountryRates.get(exchangeKey) : 1.0D;

        return getCountryFormatPrice(countryLocalPriceRuleEnum, NumberUtil.mul(defaultPrice, rate));

    }

    private static Double getCountryFormatPrice(CountryLocalPriceRuleEnum countryLocalPriceRuleEnum, Double price) {
        LocalPriceRuleEnum localPriceRuleEnum = countryLocalPriceRuleEnum.getLocalPriceRuleEnum();
        switch (localPriceRuleEnum) {
            case ROUND_TO_DECIMAL:
                return MathUtil.roundN(price, 2);
            case ROUND_TO_NUMBER:
                return MathUtil.roundN(price, 0);
            case ROUND_UP_TO_NUMBER:
                return Math.ceil(price);
        }
        return null;
    }


    private static List<SkuCountryPriceDTO> initSkuDTOsNew(List<KafkaSkuPriceDTO> sKuPriceDTOS, CountryLocalPriceRuleEnum countryLocalPriceRuleEnum, Map<String, Double> rates) {

        if (BaseUtil.isEmpty(sKuPriceDTOS)) {
            return Lists.newArrayList();
        }

        return sKuPriceDTOS.stream().map((skuPrice) ->
                new SkuCountryPriceDTO()
                        .setSkuId(skuPrice.getSkuId())
                        .setDefaultPrice(MathUtil.roundN(skuPrice.getPrice(), 2))
                        .setLocalPrice(BaseUtil.isEmpty(skuPrice.getPrice()) ? null : getLocalPriceNew(skuPrice.getPrice().doubleValue(), countryLocalPriceRuleEnum, rates))
        ).collect(Collectors.toList());
    }

}
