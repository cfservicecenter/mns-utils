package com.clubfactory.center.util;

import com.alibaba.fastjson.TypeReference;
import com.clubfactory.center.common.util.JSONUtil;
import com.clubfactory.center.dto.kafka.price.KafkaPriceDTO;
import com.clubfactory.center.oldDto.ProductPriceDTO;

import java.math.BigDecimal;
import java.util.Map;

/**
 * description:
 *
 * @author wyh
 * @date 2019/11/12 16:18
 */
public class AdapterUtilTest {


    static String exchangeRate = "{\n" +
            "            \"AED\": 3.672877,\n" +
            "            \"AFN\": 78.323326,\n" +
            "            \"ALL\": 111.505394,\n" +
            "            \"AMD\": 477.538659,\n" +
            "            \"ANG\": 1.73332,\n" +
            "            \"AOA\": 461.1155,\n" +
            "            \"ARS\": 59.591771,\n" +
            "            \"AUD\": 1.459843,\n" +
            "            \"AWG\": 1.8,\n" +
            "            \"AZN\": 1.7025,\n" +
            "            \"BAM\": 1.772051,\n" +
            "            \"BBD\": 2,\n" +
            "            \"BDT\": 84.66204,\n" +
            "            \"BGN\": 1.772675,\n" +
            "            \"BHD\": 0.376981,\n" +
            "            \"BIF\": 1872.917077,\n" +
            "            \"BMD\": 1,\n" +
            "            \"BND\": 1.360025,\n" +
            "            \"BOB\": 6.908211,\n" +
            "            \"BRL\": 4.150778,\n" +
            "            \"BSD\": 1,\n" +
            "            \"BTC\": 0.000114415325,\n" +
            "            \"BTN\": 71.486517,\n" +
            "            \"BWP\": 10.918835,\n" +
            "            \"BYN\": 2.046217,\n" +
            "            \"BZD\": 2.013772,\n" +
            "            \"CAD\": 1.323299,\n" +
            "            \"CDF\": 1663.702351,\n" +
            "            \"CHF\": 0.993356,\n" +
            "            \"CLF\": 0.024,\n" +
            "            \"CLP\": 761.000122,\n" +
            "            \"CNH\": 7.007567,\n" +
            "            \"CNY\": 7.0105,\n" +
            "            \"COP\": 3336.504501,\n" +
            "            \"CRC\": 585.014021,\n" +
            "            \"CUC\": 1,\n" +
            "            \"CUP\": 25.75,\n" +
            "            \"CVE\": 100.4,\n" +
            "            \"CZK\": 23.118529,\n" +
            "            \"DJF\": 178.05,\n" +
            "            \"DKK\": 6.772252,\n" +
            "            \"DOP\": 52.789759,\n" +
            "            \"DZD\": 120.123685,\n" +
            "            \"EGP\": 16.149645,\n" +
            "            \"ERN\": 14.999657,\n" +
            "            \"ETB\": 29.715094,\n" +
            "            \"EUR\": 0.906322,\n" +
            "            \"FJD\": 2.18668,\n" +
            "            \"FKP\": 0.777991,\n" +
            "            \"GBP\": 0.777991,\n" +
            "            \"GEL\": 2.965,\n" +
            "            \"GGP\": 0.777991,\n" +
            "            \"GHS\": 5.521824,\n" +
            "            \"GIP\": 0.777991,\n" +
            "            \"GMD\": 51.25,\n" +
            "            \"GNF\": 9147.505466,\n" +
            "            \"GTQ\": 7.695256,\n" +
            "            \"GYD\": 209.011371,\n" +
            "            \"HKD\": 7.826836,\n" +
            "            \"HNL\": 24.679179,\n" +
            "            \"HRK\": 6.743198,\n" +
            "            \"HTG\": 97.41773,\n" +
            "            \"HUF\": 302.805389,\n" +
            "            \"IDR\": 14044.428337,\n" +
            "            \"ILS\": 3.501806,\n" +
            "            \"IMP\": 0.777991,\n" +
            "            \"INR\": 71.573497,\n" +
            "            \"IQD\": 1191.977038,\n" +
            "            \"IRR\": 42105,\n" +
            "            \"ISK\": 124.7957,\n" +
            "            \"JEP\": 0.777991,\n" +
            "            \"JMD\": 140.029528,\n" +
            "            \"JOD\": 0.709,\n" +
            "            \"JPY\": 109.05242345,\n" +
            "            \"KES\": 102.388906,\n" +
            "            \"KGS\": 69.666299,\n" +
            "            \"KHR\": 4073.593921,\n" +
            "            \"KMF\": 446.249864,\n" +
            "            \"KPW\": 900,\n" +
            "            \"KRW\": 1165.459498,\n" +
            "            \"KWD\": 0.303747,\n" +
            "            \"KYD\": 0.832465,\n" +
            "            \"KZT\": 388.643855,\n" +
            "            \"LAK\": 8851.813224,\n" +
            "            \"LBP\": 1512.443526,\n" +
            "            \"LKR\": 180.398262,\n" +
            "            \"LRD\": 211.34999,\n" +
            "            \"LSL\": 14.877629,\n" +
            "            \"LYD\": 1.40627,\n" +
            "            \"MAD\": 9.670346,\n" +
            "            \"MDL\": 17.420843,\n" +
            "            \"MGA\": 3735.745809,\n" +
            "            \"MKD\": 55.734838,\n" +
            "            \"MMK\": 1516.01872,\n" +
            "            \"MNT\": 2684.361584,\n" +
            "            \"MOP\": 8.055576,\n" +
            "            \"MRO\": 357,\n" +
            "            \"MRU\": 37.204808,\n" +
            "            \"MUR\": 36.500416,\n" +
            "            \"MVR\": 15.45,\n" +
            "            \"MWK\": 733.828328,\n" +
            "            \"MXN\": 19.113626,\n" +
            "            \"MYR\": 4.1435,\n" +
            "            \"MZN\": 63.214003,\n" +
            "            \"NAD\": 14.877495,\n" +
            "            \"NGN\": 361.593636,\n" +
            "            \"NIO\": 33.723982,\n" +
            "            \"NOK\": 9.133328,\n" +
            "            \"NPR\": 114.378655,\n" +
            "            \"NZD\": 1.572015,\n" +
            "            \"OMR\": 0.384999,\n" +
            "            \"PAB\": 1,\n" +
            "            \"PEN\": 3.359243,\n" +
            "            \"PGK\": 3.392499,\n" +
            "            \"PHP\": 50.798499,\n" +
            "            \"PKR\": 155.908276,\n" +
            "            \"PLN\": 3.873033,\n" +
            "            \"PYG\": 6450.89634,\n" +
            "            \"QAR\": 3.640285,\n" +
            "            \"RON\": 4.317749,\n" +
            "            \"RSD\": 106.610778,\n" +
            "            \"RUB\": 63.881297,\n" +
            "            \"RWF\": 931.738947,\n" +
            "            \"SAR\": 3.750113,\n" +
            "            \"SBD\": 8.267992,\n" +
            "            \"SCR\": 13.700431,\n" +
            "            \"SDG\": 45.069745,\n" +
            "            \"SEK\": 9.696647,\n" +
            "            \"SGD\": 1.360672,\n" +
            "            \"SHP\": 0.777991,\n" +
            "            \"SLL\": 7438.043346,\n" +
            "            \"SOS\": 579.178783,\n" +
            "            \"SRD\": 7.458,\n" +
            "            \"SSP\": 130.26,\n" +
            "            \"STD\": 21560.79,\n" +
            "            \"STN\": 22.3,\n" +
            "            \"SVC\": 8.741506,\n" +
            "            \"SYP\": 514.974867,\n" +
            "            \"SZL\": 14.892451,\n" +
            "            \"THB\": 30.342092,\n" +
            "            \"TJS\": 9.680623,\n" +
            "            \"TMT\": 3.51,\n" +
            "            \"TND\": 2.8505,\n" +
            "            \"TOP\": 2.32028,\n" +
            "            \"TRY\": 5.773631,\n" +
            "            \"TTD\": 6.770014,\n" +
            "            \"TWD\": 30.417666,\n" +
            "            \"TZS\": 2301.455205,\n" +
            "            \"UAH\": 24.489766,\n" +
            "            \"UGX\": 3696.532557,\n" +
            "            \"USD\": 1,\n" +
            "            \"UYU\": 37.448217,\n" +
            "            \"UZS\": 9471.122737,\n" +
            "            \"VEF\": 248487.642241,\n" +
            "            \"VES\": 24211.111936,\n" +
            "            \"VND\": 23184.353041,\n" +
            "            \"VUV\": 116.201872,\n" +
            "            \"WST\": 2.64166,\n" +
            "            \"XAF\": 594.508128,\n" +
            "            \"XAG\": 0.05934693,\n" +
            "            \"XAU\": 0.00068684,\n" +
            "            \"XCD\": 2.70255,\n" +
            "            \"XDR\": 0.727877,\n" +
            "            \"XOF\": 594.508128,\n" +
            "            \"XPD\": 0.00059177,\n" +
            "            \"XPF\": 108.152959,\n" +
            "            \"XPT\": 0.00114127,\n" +
            "            \"YER\": 250.349961,\n" +
            "            \"ZAR\": 14.881726,\n" +
            "            \"ZMW\": 13.766847,\n" +
            "            \"ZWL\": 322.000001\n" +
            "        }";


    public static void main1(String[] args) throws Exception {
        String json =
                "{\n" +
                        "  \"currency\": \"CNY\",\n" +
                        "  \"id\": 1390714,\n" +
                        "  \"multiCountryPrices\": {\n" +
                        "    \"qa\": [\n" +
                        "      {\n" +
                        "        \"price\": 14.2436,\n" +
                        "        \"skuId\": 14435292,\n" +
                        "        \"skuMarkingPrice\": 20.0000\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"price\": 14.2436,\n" +
                        "        \"skuId\": 14435294,\n" +
                        "        \"skuMarkingPrice\": 20.0000\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"price\": 14.2436,\n" +
                        "        \"skuId\": 14435295,\n" +
                        "        \"skuMarkingPrice\": 20.0000\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"price\": 14.2436,\n" +
                        "        \"skuId\": 14437242,\n" +
                        "        \"skuMarkingPrice\": 20.0000\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"price\": 14.2436,\n" +
                        "        \"skuId\": 14437236,\n" +
                        "        \"skuMarkingPrice\": 20.0000\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"price\": 32.8595,\n" +
                        "        \"skuId\": 14437235,\n" +
                        "        \"skuMarkingPrice\": 20.0000\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"price\": 14.4531,\n" +
                        "        \"skuId\": 6231731,\n" +
                        "        \"skuMarkingPrice\": 20.0000\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"price\": 14.2436,\n" +
                        "        \"skuId\": 6231730,\n" +
                        "        \"skuMarkingPrice\": 20.0000\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"price\": 14.4531,\n" +
                        "        \"skuId\": 14440574,\n" +
                        "        \"skuMarkingPrice\": 20.0000\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"price\": 14.4531,\n" +
                        "        \"skuId\": 14440575,\n" +
                        "        \"skuMarkingPrice\": 20.0000\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"price\": 14.4531,\n" +
                        "        \"skuId\": 14440576,\n" +
                        "        \"skuMarkingPrice\": 20.0000\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"price\": 14.4531,\n" +
                        "        \"skuId\": 14440577,\n" +
                        "        \"skuMarkingPrice\": 20.0000\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"price\": 14.4531,\n" +
                        "        \"skuId\": 14440592,\n" +
                        "        \"skuMarkingPrice\": 20.0000\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"price\": 14.4531,\n" +
                        "        \"skuId\": 14440593,\n" +
                        "        \"skuMarkingPrice\": 20.0000\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"price\": 14.4531,\n" +
                        "        \"skuId\": 14440594,\n" +
                        "        \"skuMarkingPrice\": 20.0000\n" +
                        "      }\n" +
                        "    ],\n" +
                        "    \"ae\": [\n" +
                        "      {\n" +
                        "        \"price\": 9.7655,\n" +
                        "        \"skuId\": 14435292,\n" +
                        "        \"skuMarkingPrice\": 20.0000\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"price\": 9.7655,\n" +
                        "        \"skuId\": 14435294,\n" +
                        "        \"skuMarkingPrice\": 20.0000\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"price\": 9.7655,\n" +
                        "        \"skuId\": 14435295,\n" +
                        "        \"skuMarkingPrice\": 20.0000\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"price\": 9.7655,\n" +
                        "        \"skuId\": 14437242,\n" +
                        "        \"skuMarkingPrice\": 20.0000\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"price\": 9.7655,\n" +
                        "        \"skuId\": 14437236,\n" +
                        "        \"skuMarkingPrice\": 20.0000\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"price\": 17.2082,\n" +
                        "        \"skuId\": 14437235,\n" +
                        "        \"skuMarkingPrice\": 20.0000\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"price\": 10.1007,\n" +
                        "        \"skuId\": 6231731,\n" +
                        "        \"skuMarkingPrice\": 20.0000\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"price\": 9.7655,\n" +
                        "        \"skuId\": 6231730,\n" +
                        "        \"skuMarkingPrice\": 20.0000\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"price\": 10.1007,\n" +
                        "        \"skuId\": 14440574,\n" +
                        "        \"skuMarkingPrice\": 20.0000\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"price\": 10.1007,\n" +
                        "        \"skuId\": 14440575,\n" +
                        "        \"skuMarkingPrice\": 20.0000\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"price\": 10.1007,\n" +
                        "        \"skuId\": 14440576,\n" +
                        "        \"skuMarkingPrice\": 20.0000\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"price\": 10.1007,\n" +
                        "        \"skuId\": 14440577,\n" +
                        "        \"skuMarkingPrice\": 20.0000\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"price\": 10.1007,\n" +
                        "        \"skuId\": 14440592,\n" +
                        "        \"skuMarkingPrice\": 20.0000\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"price\": 10.1007,\n" +
                        "        \"skuId\": 14440593,\n" +
                        "        \"skuMarkingPrice\": 20.0000\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"price\": 10.1007,\n" +
                        "        \"skuId\": 14440594,\n" +
                        "        \"skuMarkingPrice\": 20.0000\n" +
                        "      }\n" +
                        "    ],\n" +
                        "    \"in\": [\n" +
                        "      {\n" +
                        "        \"price\": 2.5437,\n" +
                        "        \"skuId\": 14435292,\n" +
                        "        \"skuMarkingPrice\": 20.0000\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"price\": 2.5437,\n" +
                        "        \"skuId\": 14435294,\n" +
                        "        \"skuMarkingPrice\": 20.0000\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"price\": 2.5437,\n" +
                        "        \"skuId\": 14435295,\n" +
                        "        \"skuMarkingPrice\": 20.0000\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"price\": 2.5437,\n" +
                        "        \"skuId\": 14437242,\n" +
                        "        \"skuMarkingPrice\": 20.0000\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"price\": 2.5437,\n" +
                        "        \"skuId\": 14437236,\n" +
                        "        \"skuMarkingPrice\": 20.0000\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"price\": 3.6787,\n" +
                        "        \"skuId\": 14437235,\n" +
                        "        \"skuMarkingPrice\": 20.0000\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"price\": 2.5994,\n" +
                        "        \"skuId\": 6231731,\n" +
                        "        \"skuMarkingPrice\": 20.0000\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"price\": 2.5437,\n" +
                        "        \"skuId\": 6231730,\n" +
                        "        \"skuMarkingPrice\": 20.0000\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"price\": 2.5994,\n" +
                        "        \"skuId\": 14440574,\n" +
                        "        \"skuMarkingPrice\": 20.0000\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"price\": 2.5994,\n" +
                        "        \"skuId\": 14440575,\n" +
                        "        \"skuMarkingPrice\": 20.0000\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"price\": 2.5994,\n" +
                        "        \"skuId\": 14440576,\n" +
                        "        \"skuMarkingPrice\": 20.0000\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"price\": 2.5994,\n" +
                        "        \"skuId\": 14440577,\n" +
                        "        \"skuMarkingPrice\": 20.0000\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"price\": 2.5994,\n" +
                        "        \"skuId\": 14440592,\n" +
                        "        \"skuMarkingPrice\": 20.0000\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"price\": 2.5994,\n" +
                        "        \"skuId\": 14440593,\n" +
                        "        \"skuMarkingPrice\": 20.0000\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"price\": 2.5994,\n" +
                        "        \"skuId\": 14440594,\n" +
                        "        \"skuMarkingPrice\": 20.0000\n" +
                        "      }\n" +
                        "    ],\n" +
                        "    \"gb\": [\n" +
                        "      {\n" +
                        "        \"price\": 10.5015,\n" +
                        "        \"skuId\": 14435292,\n" +
                        "        \"skuMarkingPrice\": 20.0000\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"price\": 10.5015,\n" +
                        "        \"skuId\": 14435294,\n" +
                        "        \"skuMarkingPrice\": 20.0000\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"price\": 10.5015,\n" +
                        "        \"skuId\": 14435295,\n" +
                        "        \"skuMarkingPrice\": 20.0000\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"price\": 10.5015,\n" +
                        "        \"skuId\": 14437242,\n" +
                        "        \"skuMarkingPrice\": 20.0000\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"price\": 10.5015,\n" +
                        "        \"skuId\": 14437236,\n" +
                        "        \"skuMarkingPrice\": 20.0000\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"price\": 18.2176,\n" +
                        "        \"skuId\": 14437235,\n" +
                        "        \"skuMarkingPrice\": 20.0000\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"price\": 10.7995,\n" +
                        "        \"skuId\": 6231731,\n" +
                        "        \"skuMarkingPrice\": 20.0000\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"price\": 10.5015,\n" +
                        "        \"skuId\": 6231730,\n" +
                        "        \"skuMarkingPrice\": 20.0000\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"price\": 10.7995,\n" +
                        "        \"skuId\": 14440574,\n" +
                        "        \"skuMarkingPrice\": 20.0000\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"price\": 10.7995,\n" +
                        "        \"skuId\": 14440575,\n" +
                        "        \"skuMarkingPrice\": 20.0000\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"price\": 10.7995,\n" +
                        "        \"skuId\": 14440576,\n" +
                        "        \"skuMarkingPrice\": 20.0000\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"price\": 10.7995,\n" +
                        "        \"skuId\": 14440577,\n" +
                        "        \"skuMarkingPrice\": 20.0000\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"price\": 10.7995,\n" +
                        "        \"skuId\": 14440592,\n" +
                        "        \"skuMarkingPrice\": 20.0000\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"price\": 10.7995,\n" +
                        "        \"skuId\": 14440593,\n" +
                        "        \"skuMarkingPrice\": 20.0000\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"price\": 10.7995,\n" +
                        "        \"skuId\": 14440594,\n" +
                        "        \"skuMarkingPrice\": 20.0000\n" +
                        "      }\n" +
                        "    ],\n" +
                        "    \"mx\": [\n" +
                        "      {\n" +
                        "        \"price\": 0.8974,\n" +
                        "        \"skuId\": 14435292,\n" +
                        "        \"skuMarkingPrice\": 20.0000\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"price\": 0.8974,\n" +
                        "        \"skuId\": 14435294,\n" +
                        "        \"skuMarkingPrice\": 20.0000\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"price\": 0.8974,\n" +
                        "        \"skuId\": 14435295,\n" +
                        "        \"skuMarkingPrice\": 20.0000\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"price\": 0.8974,\n" +
                        "        \"skuId\": 14437242,\n" +
                        "        \"skuMarkingPrice\": 20.0000\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"price\": 0.8974,\n" +
                        "        \"skuId\": 14437236,\n" +
                        "        \"skuMarkingPrice\": 20.0000\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"price\": 3.8628,\n" +
                        "        \"skuId\": 14437235,\n" +
                        "        \"skuMarkingPrice\": 20.0000\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"price\": 0.9510,\n" +
                        "        \"skuId\": 6231731,\n" +
                        "        \"skuMarkingPrice\": 20.0000\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"price\": 0.8974,\n" +
                        "        \"skuId\": 6231730,\n" +
                        "        \"skuMarkingPrice\": 20.0000\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"price\": 0.9510,\n" +
                        "        \"skuId\": 14440574,\n" +
                        "        \"skuMarkingPrice\": 20.0000\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"price\": 0.9510,\n" +
                        "        \"skuId\": 14440575,\n" +
                        "        \"skuMarkingPrice\": 20.0000\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"price\": 0.9510,\n" +
                        "        \"skuId\": 14440576,\n" +
                        "        \"skuMarkingPrice\": 20.0000\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"price\": 0.9510,\n" +
                        "        \"skuId\": 14440577,\n" +
                        "        \"skuMarkingPrice\": 20.0000\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"price\": 0.9510,\n" +
                        "        \"skuId\": 14440592,\n" +
                        "        \"skuMarkingPrice\": 20.0000\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"price\": 0.9510,\n" +
                        "        \"skuId\": 14440593,\n" +
                        "        \"skuMarkingPrice\": 20.0000\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"price\": 0.9510,\n" +
                        "        \"skuId\": 14440594,\n" +
                        "        \"skuMarkingPrice\": 20.0000\n" +
                        "      }\n" +
                        "    ],\n" +
                        "    \"sa\": [\n" +
                        "      {\n" +
                        "        \"price\": 15.5409,\n" +
                        "        \"skuId\": 14435292,\n" +
                        "        \"skuMarkingPrice\": 20.0000\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"price\": 15.5409,\n" +
                        "        \"skuId\": 14435294,\n" +
                        "        \"skuMarkingPrice\": 20.0000\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"price\": 15.5409,\n" +
                        "        \"skuId\": 14435295,\n" +
                        "        \"skuMarkingPrice\": 20.0000\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"price\": 15.5409,\n" +
                        "        \"skuId\": 14437242,\n" +
                        "        \"skuMarkingPrice\": 20.0000\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"price\": 15.5409,\n" +
                        "        \"skuId\": 14437236,\n" +
                        "        \"skuMarkingPrice\": 20.0000\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"price\": 29.8681,\n" +
                        "        \"skuId\": 14437235,\n" +
                        "        \"skuMarkingPrice\": 20.0000\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"price\": 15.9810,\n" +
                        "        \"skuId\": 6231731,\n" +
                        "        \"skuMarkingPrice\": 20.0000\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"price\": 15.5409,\n" +
                        "        \"skuId\": 6231730,\n" +
                        "        \"skuMarkingPrice\": 20.0000\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"price\": 15.9810,\n" +
                        "        \"skuId\": 14440574,\n" +
                        "        \"skuMarkingPrice\": 20.0000\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"price\": 15.9810,\n" +
                        "        \"skuId\": 14440575,\n" +
                        "        \"skuMarkingPrice\": 20.0000\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"price\": 15.9810,\n" +
                        "        \"skuId\": 14440576,\n" +
                        "        \"skuMarkingPrice\": 20.0000\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"price\": 15.9810,\n" +
                        "        \"skuId\": 14440577,\n" +
                        "        \"skuMarkingPrice\": 20.0000\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"price\": 15.9810,\n" +
                        "        \"skuId\": 14440592,\n" +
                        "        \"skuMarkingPrice\": 20.0000\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"price\": 15.9810,\n" +
                        "        \"skuId\": 14440593,\n" +
                        "        \"skuMarkingPrice\": 20.0000\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"price\": 15.9810,\n" +
                        "        \"skuId\": 14440594,\n" +
                        "        \"skuMarkingPrice\": 20.0000\n" +
                        "      }\n" +
                        "    ],\n" +
                        "    \"us\": [\n" +
                        "      {\n" +
                        "        \"price\": 9.6474,\n" +
                        "        \"skuId\": 14435292,\n" +
                        "        \"skuMarkingPrice\": 20.0000\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"price\": 9.6474,\n" +
                        "        \"skuId\": 14435294,\n" +
                        "        \"skuMarkingPrice\": 20.0000\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"price\": 9.6474,\n" +
                        "        \"skuId\": 14435295,\n" +
                        "        \"skuMarkingPrice\": 20.0000\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"price\": 9.6474,\n" +
                        "        \"skuId\": 14437242,\n" +
                        "        \"skuMarkingPrice\": 20.0000\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"price\": 9.6474,\n" +
                        "        \"skuId\": 14437236,\n" +
                        "        \"skuMarkingPrice\": 20.0000\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"price\": 28.4887,\n" +
                        "        \"skuId\": 14437235,\n" +
                        "        \"skuMarkingPrice\": 20.0000\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"price\": 10.0355,\n" +
                        "        \"skuId\": 6231731,\n" +
                        "        \"skuMarkingPrice\": 20.0000\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"price\": 9.6474,\n" +
                        "        \"skuId\": 6231730,\n" +
                        "        \"skuMarkingPrice\": 20.0000\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"price\": 10.0355,\n" +
                        "        \"skuId\": 14440574,\n" +
                        "        \"skuMarkingPrice\": 20.0000\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"price\": 10.0355,\n" +
                        "        \"skuId\": 14440575,\n" +
                        "        \"skuMarkingPrice\": 20.0000\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"price\": 10.0355,\n" +
                        "        \"skuId\": 14440576,\n" +
                        "        \"skuMarkingPrice\": 20.0000\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"price\": 10.0355,\n" +
                        "        \"skuId\": 14440577,\n" +
                        "        \"skuMarkingPrice\": 20.0000\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"price\": 10.0355,\n" +
                        "        \"skuId\": 14440592,\n" +
                        "        \"skuMarkingPrice\": 20.0000\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"price\": 10.0355,\n" +
                        "        \"skuId\": 14440593,\n" +
                        "        \"skuMarkingPrice\": 20.0000\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"price\": 10.0355,\n" +
                        "        \"skuId\": 14440594,\n" +
                        "        \"skuMarkingPrice\": 20.0000\n" +
                        "      }\n" +
                        "    ],\n" +
                        "    \"others\": [\n" +
                        "      {\n" +
                        "        \"price\": 9.4916,\n" +
                        "        \"skuId\": 14435292,\n" +
                        "        \"skuMarkingPrice\": 20.0000\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"price\": 9.4916,\n" +
                        "        \"skuId\": 14435294,\n" +
                        "        \"skuMarkingPrice\": 20.0000\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"price\": 9.4916,\n" +
                        "        \"skuId\": 14435295,\n" +
                        "        \"skuMarkingPrice\": 20.0000\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"price\": 9.4916,\n" +
                        "        \"skuId\": 14437242,\n" +
                        "        \"skuMarkingPrice\": 20.0000\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"price\": 9.4916,\n" +
                        "        \"skuId\": 14437236,\n" +
                        "        \"skuMarkingPrice\": 20.0000\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"price\": 28.0984,\n" +
                        "        \"skuId\": 14437235,\n" +
                        "        \"skuMarkingPrice\": 20.0000\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"price\": 9.8237,\n" +
                        "        \"skuId\": 6231731,\n" +
                        "        \"skuMarkingPrice\": 20.0000\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"price\": 9.4916,\n" +
                        "        \"skuId\": 6231730,\n" +
                        "        \"skuMarkingPrice\": 20.0000\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"price\": 9.8237,\n" +
                        "        \"skuId\": 14440574,\n" +
                        "        \"skuMarkingPrice\": 20.0000\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"price\": 9.8237,\n" +
                        "        \"skuId\": 14440575,\n" +
                        "        \"skuMarkingPrice\": 20.0000\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"price\": 9.8237,\n" +
                        "        \"skuId\": 14440576,\n" +
                        "        \"skuMarkingPrice\": 20.0000\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"price\": 9.8237,\n" +
                        "        \"skuId\": 14440577,\n" +
                        "        \"skuMarkingPrice\": 20.0000\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"price\": 9.8237,\n" +
                        "        \"skuId\": 14440592,\n" +
                        "        \"skuMarkingPrice\": 20.0000\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"price\": 9.8237,\n" +
                        "        \"skuId\": 14440593,\n" +
                        "        \"skuMarkingPrice\": 20.0000\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"price\": 9.8237,\n" +
                        "        \"skuId\": 14440594,\n" +
                        "        \"skuMarkingPrice\": 20.0000\n" +
                        "      }\n" +
                        "    ]\n" +
                        "  },\n" +
                        "  \"overwrite\": false,\n" +
                        "  \"productNo\": \"CNN001390714N\"\n" +
                        "}";


        ProductPriceDTO result = AdapterUtil.adaptPrice(
                JSONUtil.string2Object(json, KafkaPriceDTO.class),
                JSONUtil.string2Object(exchangeRate, new TypeReference<Map<String, Double>>() {
                }));

    }

    public static void main(String[] args) throws Exception {
        BigDecimal result = AdapterUtil.getPriceWithCountry(new BigDecimal(666), "CNY", "in", JSONUtil.string2Object(exchangeRate, new TypeReference<Map<String, Double>>() {
        }));
    }
}
